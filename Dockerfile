FROM node

MAINTAINER OscaR
LABEL OscaR created this image for test Docker

ARG arg_port=3000

ENV wdir="/home/oscar/app"
ENV port=$arg_port

run useradd -s /bin/bash oscar
USER oscar

RUN mkdir $wdir
WORKDIR $wdir

COPY package.json $wdir
RUN yarn install

COPY . $wdir

#RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE $port
